/*
 * application to show info of the batteries of the system under
 *  SBM3
 *
 * (c) 2014, congatec AG, Alex de Cabo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "i2c-dev.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <dirent.h>
#include <fcntl.h>

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

#define BASE_UNIT_CONVERSION            1000
#define BATTERY_MODE_CAP_MULT_WATT      (10 * BASE_UNIT_CONVERSION)
#define TIME_UNIT_CONVERSION            60
#define TEMP_KELVIN_TO_CELSIUS          2731

/* battery status value bits */
#define BATTERY_DISCHARGING             0x40
#define BATTERY_FULL_CHARGED            0x20
#define BATTERY_FULL_DISCHARGED         0x10

#define version	"1.0"

char *function[] = {
	"Manufacturer Access",
	"Remaining Capacity Alarm",
	"Remaining Time Alarm",
	"Battery Mode",
	"At Rate",
	"At Rate Time To Full",
	"At Rate Time To Empty",
	"At Rate OK",
	"Temperature [C]",
	"Voltage [mV]",
	"Current [mA]",
	"Average Current",
	"Max Error",
	"Relative State Of Charge [%]",
	"Absolute State Of Charge [%]",
	"Remaining Capacity [mAh]",
	"FullCharge Capacity [mAh]",
	"Run Time To Empty",
	"Average Time To Empty [time]",
	"Average Time To Full [time]",
	"Charging Current",
	"Charging Voltage",
	"Battery Status",
	"Cycle Count",
	"Design Capacity",
	"Design Voltage",
	"Specification Info",
	"Manufacture Date",
	"Serial Number",
	"Reserved",
	"Reserved",
	"Reserved",
	"Manufacturer Name",
	"Device Name",
	"Device Chemistry",
	"Manufacturer Data",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Firmware Revision",
	"Optional MfgCfgReg1",
	"Optional MfgCfgReg2",
	"Optional MfgCfgReg3",
	"Reserved",
	"Battery System State",
	"Battery System State Cont",
	"Reserved",
	"Battery System Info",
};

void usage()
{
	fprintf(stdout,
		"\ncongatec info of the batteries under SBM3, version %s\n",
		version);
	fprintf(stdout, "(c)2014, congatec AG \n\n");
	fprintf(stdout, "Usage: sbm3 [-l]\n\n");
	fprintf(stdout, "Options:\n");
	fprintf(stdout, " -h\t\thelp\n");
	fprintf(stdout, " -l\t\tendless loop\n");
}

void show_battery_info(int file, int *aRegisters)
{
	int i;
	int res;
	printf("\n\n");
	for (i = 0; i < 8; i++) {
		res = i2c_smbus_read_word_data(file, aRegisters[i]);
		if (res < 0) {
			/* ERROR HANDLING: i2c transaction failed */
			printf("\n%s ! Error", function[aRegisters[i]]);
		} else {
			printf("\t%s: ", function[aRegisters[i]]);
			/* res contains the read word */
			switch (aRegisters[i]) {
			case 0x08:
				if (res > 0 && res < 65535)
					printf("%.1f\n", (float)res / 10 - 273);
				else
					printf("-\n");
				break;
			case 0x12:
			case 0x13:
				if (res > 0 && res < 65535)
					printf("%d:%.2d\n", res / 60, res % 60);
				else
					printf("-\n");
				break;
			case 0x16:
				if (res > 0 && res < 65535) {
					if (res & BATTERY_FULL_CHARGED)
						printf("Full \n");
					else if (res & BATTERY_FULL_DISCHARGED)
						printf("Not Charging \n");
					else if (res & BATTERY_DISCHARGING)
						printf("Discharging\n");
					else
						printf("Charging\n");
				} else
					printf("-\n");
				break;
			default:
				if (res > 0 && res < 65535)
					printf("%d\n", res);
				else
					printf("-\n");
				break;
			}
		}
	}
	printf("\n\n");
}

int main(int argc, char *argv[])
{
	int file, opt;
	int i2cbus = 0;
	char filename[20] = "/dev/i2c-0";
	int addr = 0x0b;
	__s32 res;
	__s32 tmp;
	int numBat = 0;
	int loop = 0;

	/* Registers to show */
	int aRegisters[] = { 0x08,	// Temp
		0x0D,		// Capacity
		0x0E,		// Remaining capacity
		0x0F,		// Remaining capacity charge
		0x10,		// Full charge capacity
		0x12,		// Time to empty
		0x13,		// Time to full
		0x16,		// Status
	};

	while ((opt = getopt(argc, argv, "hl")) != -1) {
		switch (opt) {
		case 'h':
			usage();
			exit(0);

		case 'l':
			loop = 1;
			break;

		default:
			break;
		}
	}
	file = open(filename, O_RDWR);

	if (file < 0 && errno == ENOENT) {
		sprintf(filename, "/dev/i2c-%d", i2cbus);
		file = open(filename, O_RDWR);
	}

	if (file < 0) {
		if (errno == ENOENT) {
			fprintf(stderr, "Error: Could not open file "
				"`/dev/i2c-%d' or `/dev/i2c/%d': %s\n",
				i2cbus, i2cbus, strerror(ENOENT));
		} else {
			fprintf(stderr, "Error: Could not open file "
				"`%s': %s\n", filename, strerror(errno));
			if (errno == EACCES)
				fprintf(stderr, "Run as root?\n");
		}
	}
	if (ioctl(file, I2C_SLAVE, addr) < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}

	while (1) {
		res = i2c_smbus_read_word_data(file, 0x41);	// Read Status
		numBat = res & 0x000F;

		switch (numBat) {
		case 1:
		case 2:
			tmp = res;
			res = (res & 0x0FFF) + (numBat * 4096);	// Set highest nibble to the value
			i2c_smbus_write_word_data(file, 0x41, res);	//  of numBat
			printf("\n Battery %d:", numBat);
			show_battery_info(file, aRegisters);
			i2c_smbus_write_word_data(file, 0x41, tmp);
			break;
		case 3:
			printf("\n Battery 1:");
			show_battery_info(file, aRegisters);
			tmp = res;
			res += 4096;
			i2c_smbus_write_word_data(file, 0x41, res);	// Set highest nibble to 2 in order
			printf(" Battery 2:");	//  to get info from bat2
			show_battery_info(file, aRegisters);
			i2c_smbus_write_word_data(file, 0x41, tmp);	// Restor the value of register 0x41
			break;
		default:
			printf("\n***\n* No battery detected [Error]\n***\n\n");
			loop = 0;
			break;
		}

		if (loop != 1)
			break;
		else
			sleep(5);
	}

	close(file);

	exit(0);
}
