/*
 * daemon for monitoring events issued by conga-QMX6 gpio button signals.
 *
 * (c) 2014, congatec AG, sml
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <linux/input.h>
#include <syslog.h>
#include <sys/stat.h>

/* macros taken from evtest.c http://cgit.freedesktop.org/~whot/evtest/ */
#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) ((((x)-1)/BITS_PER_LONG)+1)
#define OFF(x) ((x)%BITS_PER_LONG)
#define BIT(x) (1UL<<OFF(x))
#define LONG(x) ((x)/BITS_PER_LONG)
#define test_bit(bit, array)        ((array[LONG(bit)] >> OFF(bit)) & 1)

#define BTN_PWROFF	116
#define BTN_SLEEP	142

#define FLAG_POWEROFF	0x01
#define FLAG_MEMORY		0x02
#define FLAG_STANDBY	0x04

#define version	"1.0"

char cmd_poweroff[128] = { "/sbin/poweroff" };


void usage()
{
	fprintf(stdout, "\ncongatec button event handler, version %s\n", version);
	fprintf(stdout, "(c)2014, congatec AG, sml\n\n");
	fprintf(stdout, "Usage: btevent [-m|-s] [-p]\n\n");
	fprintf(stdout, "Options:\n");
	fprintf(stdout, " -h\t\thelp\n");
	fprintf(stdout, " -m\t\tsleep signal causes suspend to RAM\n");
	fprintf(stdout, " -s\t\tsleep signal causes standby\n");
	fprintf(stdout, " -p\t\tsystem poweroff via power button signal\n");
	fprintf(stdout, " -x COMMAND\texecute COMMAND instead of /sbin/poweroff\n\t\tin case of power button event\n\n");
}

char *gnu_basename(char *path)
{
    char *base = strrchr(path, '/');
    return base ? base+1 : path;
}

int watch_events(int fd, int num_buttons, unsigned int flags)
{
	FILE *fPowerState;
	int size, len, i;
	struct input_event *events;
	size = sizeof(struct input_event) * num_buttons * 4;
	int toggle[num_buttons];
	
	events = malloc(size);

	if (!events)
		return -1;

	memset(toggle, 0, num_buttons * sizeof(int));

	while (1) {
		memset(events, 0, size);

		len = read(fd, events, size);

		if (len < sizeof(struct input_event))
			break;

		len /= sizeof(struct input_event);

		for (i = 0; i < len; i++) {
			if (events[i].type == EV_KEY) {
				if (events[i].value)
					toggle[i] = 1;

				switch (events[i].code) {
				case BTN_PWROFF:
				
					/* skip this event if it is not enabled */
					if (!(flags & FLAG_POWEROFF))
						break;
				
					if (!events[i].value && toggle[i]) {
						toggle[i] = 0;
						syslog(LOG_INFO, "pwroff event detected");						
						execl(cmd_poweroff, gnu_basename(cmd_poweroff), NULL);
					}
					break;

				case BTN_SLEEP:

					/* skip this event if it is not enabled */
					if (!(flags & (FLAG_STANDBY|FLAG_MEMORY)))
						break;

					if (!events[i].value && toggle[i]) {
						syslog(LOG_INFO, "sleep event detected");						
						toggle[i] = 0;
						fPowerState = fopen("/sys/power/state", "w");
						if (fPowerState != NULL) {
							if (flags & FLAG_STANDBY)
								fwrite("standby\n", 1, 8, fPowerState);
							else
								fwrite("mem\n", 1, 4, fPowerState);
						}
						else
							fprintf(stdout, "Error opening /sys/power/state\n");
						fclose(fPowerState);
					}
					break;
				}
			}
		}
	}

	free(events);
	return len;
}

int main(int argc, char *argv[])
{
	int opt, fd, i, num_buttons;
	unsigned long buttons[NBITS(KEY_MAX)];
	unsigned int flags = 0;
	pid_t pid, sid;

	/* parse command line options */
	while ((opt = getopt(argc, argv, "hmspx:")) != -1) {

		switch (opt) {
			case 'h':
				usage();
				exit(0);
				
			case 'm':
				flags |= FLAG_MEMORY;
				break;
				
			case 's':
				flags |= FLAG_STANDBY;
				break;
				
			case 'p':
				flags |= FLAG_POWEROFF;
				break;
				
			case 'x':
				if (strlen(optarg) > 0 && strlen(optarg) < sizeof(cmd_poweroff)) {
					if (strlen(gnu_basename(optarg)) == 0) {
						fprintf(stdout,"basename of argument has zero size\n");
						exit(1);
					}					
					strcpy(cmd_poweroff,optarg);
				}	
				else
				{
					fprintf(stdout,"argument for poweroff command too long (max. 128)\n");
					exit(1);
				}	
				break;
				
			default:
				break;
		}
	}

	pid = fork();

	if (pid < 0)
		exit(EXIT_FAILURE);

	if (pid > 0) {
		/* exit parent process */
		exit(EXIT_SUCCESS);
	}

	/* child process created successfully */
	umask(0);

	openlog("btevent", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

	sid = setsid();
	if (sid < 0) {
		syslog(LOG_INFO, "error setting sid");
		closelog();
		exit(EXIT_FAILURE);
	}

	/* change current working directory */
	if ((chdir("/")) < 0) {
		syslog(LOG_INFO, "error changing working directory");
		closelog();
		exit(EXIT_FAILURE);
	}

	/* close the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	fd = open("/dev/input/by-path/platform-gpio-keys-event", O_RDONLY);

	if (fd < 0) {
		syslog(LOG_INFO, "error opening /dev/input/by-path/platform-gpio-keys-event");
		closelog();
		exit(EXIT_FAILURE);
	}

	memset(buttons, 0, sizeof(buttons));

	if (ioctl(fd, EVIOCGBIT(EV_KEY, KEY_MAX), buttons) < 0) {
		syslog(LOG_INFO, "error EVIOCGBIT");
		close(fd);
		closelog();
		exit(EXIT_FAILURE);
	}

	num_buttons = 0;
	for (i = 0; i < KEY_MAX; i++) {
		if (test_bit(i, buttons)) {
			num_buttons++;
		}
	}
	
	syslog(LOG_INFO, "monitoring button events");
	watch_events(fd, num_buttons, flags);
	close(fd);
	closelog();

	exit(EXIT_SUCCESS);
}
